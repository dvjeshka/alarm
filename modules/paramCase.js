export default function paramCase(text) {
    if (!text) return
    return text
        .split(/(?=[A-Z])/g)
        .map(function(value) {
            return value.charAt(0).toLowerCase() + value.substring(1)
        })
        .join('-')
}
