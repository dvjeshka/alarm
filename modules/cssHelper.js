const jspx = ($target, $context = FONT_SIZE_PX.main) => {
    return $target / $context + 'em'
}

const H_SIZE_PX = {
    h1: 42,
    h2: 36,
    h3: 28,
    h4: 24,
    h5: 20,
    h6: 18
}


const FONT_SIZE_PX = {
    ...H_SIZE_PX,
    main: 16,
    small: 14
}


const BREAKPOINTS = {
    xs: '600px'
}

const cssVars = {
    'point-xs': 600,
    'point-sm': 960,
    'point-md': 1024,

    'border-radius': 20,
    'border-radius-field': 10,
    'box-shadow': '0px 20px 20px rgba(0, 0, 0, 0.1)',
    'box-shadow-decor': '0px 0px 20px rgba(0, 25, 255, .5);',
    'linear-gradient':'linear-gradient(289.25deg, #BFD7DF -5.27%, #f4f7fa 64.17%)',
    'grid-container': 1250,
    gap: 30,
    'gap-sm': 15,
    'gap-sm-n': -15,
    'indent-xs':jspx(15),
    'indent-sm':jspx(30),
    'indent-md':jspx(50),
    'indent-mlg':jspx(75),
    'indent-lg':jspx(100),
    'indent-xlg':jspx(150)



}

const COLORS = {
    error: '#ff0000',
    border: '#E4E4E4',
    accent: '#293DF2',
    accentdark: '#612DB5',
    accentlight: '#ED662C',
    dark: '#00003E',
    info: '#EDF2F7',
    infodark: '#7C99BA',
    infodarken: '#BFCEDF',

    light: 'white',
    primary: 'black',
    secondary: 'blue'
}

/* primary: '#1976D2',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107', */





const BODY_FONT = {
    fontStyle: 'normal',
    fontWeight: 400,
    fontSize: '16px',
    fontFamily: 'PT Sans, Helvetica, Arial, sans-serif',

    lineHeight: 1.5
}








const jscolor = (name) => `var(--color-${name})`
const jsfs = (name) => `var(--fontsize-${name})`


const jsvar = (key) => `var(--${key})`

const postcssFunctions = {
    functions: {
        jscolor,
        jsvar,
        jspx,
        jsfs,
 

    }
}

const jsvarlist = () => {
    const newKeys = {}
    for (const [key, value] of Object.entries(COLORS)) {
        newKeys[`--color-${key}`] = value
    }

    for (const [key, value] of Object.entries(FONT_SIZE_PX)) {
        if (key == 'main') newKeys[`--fontsize-${key}`] = value + 'px'
        else newKeys[`--fontsize-${key}`] = jspx(value)
    }

    for (let [key, value] of Object.entries(cssVars)) {
        if (Number.isInteger(value)) value = value + 'px'
        newKeys[`--${key}`] = value
    }

    return newKeys
}



const postcssMixins = {
    mixins: {
        normalizeBtn: {
            'background-color': 'transparent',
            'font-family': 'inherit' /* 1 */,
            'font-size': '100%' /* 1 */,
            'line-height': '1.15' /* 1 */,
            margin: '0' /* 2 */,
            overflow: 'visible',
            'text-transform': 'none',
            height: 'auto',
            '&,& *': { cursor: 'pointer' },
            '&[type="button"],&[type="reset"],&[type="submit"]': {
                '-webkit-appearance': 'button'
            },
            '&:-moz-focusring,&[type="button"]:-moz-focusring,&[type="reset"]:-moz-focusring,&[type="submit"]:-moz-focusring': {
                outline: '1px dotted ButtonText'
            },
            '&::-webkit-file-upload-button': {
                '-webkit-appearance': 'button',
                font: 'inherit'
            }
        },

        point(mixin, name) {
            const obj = {}
            if (name === 'xs-after')
                obj[`@media (min-width:${cssVars['point-xs'] + 1}px)`] = {
                    '@mixin-content': {}
                }
            if (name === 'xs-only')
                obj[`@media (max-width:${cssVars['point-xs']}px)`] = {
                    '@mixin-content': {}
                }
            if (name === 'sm-after')
                obj[`@media (min-width:${cssVars['point-sm'] + 1}px)`] = {
                    '@mixin-content': {}
                }
            if (name === 'sm-only')
                obj[`@media (max-width:${cssVars['point-sm']}px)`] = {
                    '@mixin-content': {}
                }
            if (name === 'md-only')
                obj[`@media (max-width:${cssVars['point-md']}px)`] = {
                    '@mixin-content': {}
                }

            if (name === 'xs-sm')
                obj[
                    `@media (min-width:${cssVars['point-xs'] +
                        1}px) and (max-width:${cssVars['point-sm']}px)`
                ] = { '@mixin-content': {} }

            return obj
        },
        h(mixin, name) {
            const [level = '1', color = 'dark'] = name.split('-')

            const cssPropery = {
                'font-family': 'Oswald',
                'line-height': 1.5,
                'font-weight': 400,
                color: jscolor(color),
                'margin-top': 0,
                'margin-bottom': '1em',
       
            }

            if (level == '1' || level == '2')
                cssPropery['text-transform'] = 'uppercase'

            cssPropery[`@media (max-width:${cssVars['point-xs']}px)`]={
                'word-break':'break-word'
            }


            /* if(level=='1') {
                cssPropery[`@media (min-width:${cssVars['point-xs']+1}px)`]={
                   'font-size':jsfs('h'+level)
                }
                cssPropery['font-size']=jsfs('h'+5)
            }
            else */
            cssPropery['font-size'] = jsfs('h' + level)

            return cssPropery
        },
        jsCssVar(){
          return jsvarlist()
        },
        themeDefault() {


            return {

                html: {
                    font: `${BODY_FONT.fontStyle} ${BODY_FONT.fontWeight} ${BODY_FONT.fontSize}/${BODY_FONT.lineHeight} ${BODY_FONT.fontFamily}`
                },
                body: {
                    backgroundColor: jscolor('light'),
                    color: jscolor('infodark')
                }
            }
        }
    }
}

export { postcssFunctions, postcssMixins, jsvarlist, cssVars }
