export const state = () => ({
    asideMenuMain:{
        open:true,
        categoryIdsOpen:[]
    },
    pageTitle:'Alarm',
    pageMeta:{
        title:'AlarmSt',
        description:'',
        keywords:''
    },
    breadCrumbs:[{
            title:'Главная',
            to:{name:'index'}
            }],
    pageData:{
       
    }

})

export const mutations = {
    set(state, obj) {
        Object.assign(state,obj)

    },
    categoryIdsOpenRemoveId(state,indexId){
        state.asideMenuMain.categoryIdsOpen.splice(indexId, 1);

    },
    categoryIdsOpenAddId(state,id){
        state.asideMenuMain.categoryIdsOpen.push(id)
    },
    setAsideMenuMainToggle(state) {
        state.asideMenuMain.open = !state.asideMenuMain.open
    },
    setPageTitle(state,string) {
        state.pageTitle = string
    },
    setPageData(state,[field,val]) {
        this._vm.$set(state.pageData,field,val)
       
    },
    setPageMeta(state,{data}) {

        this._vm.$set(state.pageMeta,'title',data?.pageTitle)
        this._vm.$set(state.pageMeta,'description',data?.pageDescription)
        this._vm.$set(state.pageMeta,'keywords',data?.pageKeywords)
        
    },
    setPageClear(state) {
        state.pageData = {}
        state.pageMeta = {}
       
    },
    breadCrumbsPush(state,val){
        if(!state.breadCrumbs.find(item=>item.title===val.title)) state.breadCrumbs.push(val)
      
    },
    breadCrumbsDeleteLastItem(state){
        state.breadCrumbs.length=1
    }
    
}
