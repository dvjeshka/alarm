import Vue from 'vue'

import VContainer from '@/components/VContainer.vue'
import VTitle from '@/components/VTitle.vue'
import VBem from '@/components/VBem.vue'
import MyNuxtLink from '@/components/MyNuxtLink.vue'

import apiServer from '@/modules/apiServer.js'
import clearTypeNameField from '@/modules/clearTypeNameField.js'
if(!Vue.prototype.$_apiServer) Object.defineProperty(Vue.prototype, '$_apiServer', { value: apiServer })
if(!Vue.prototype.$_clearTypeNameField) Object.defineProperty(Vue.prototype, '$_clearTypeNameField', { value: clearTypeNameField })


import global from '@/mixins/global.js'

Vue.mixin(global)

Vue.component('v-container', VContainer)
Vue.component('v-title', VTitle)
Vue.component('v-bem', VBem)
Vue.component('my-nuxt-link', MyNuxtLink)
