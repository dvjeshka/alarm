

import gql from 'graphql-tag'

import DRAFT_TICKET_ITEM from '@/api/gql/queryDradtTicketItem.gql'



export const typeDefs = gql`
    type TicketFilter {
        is_request: Boolean
        is_free: Boolean
        is_auction: Boolean
        categories:[Int]
        type: Int
        price:Range,
        expiry_date: Range
        author_id: Int
    }
   
    type Range {
        min: Float
        max: Float
    }

    type TicketOrderBy {
        field: TicketOrderByFieldsEnum
        direction: OrderByDirectionEnum
    }
    enum TicketOrderByFieldsEnum {
        update_date
        expiry_date
        price
    }
    enum OrderByDirectionEnum {
        asc
        desc
    }
   
    
   
   

    type TicketOrderBy {
        field: TicketOrderByFieldsEnum
        direction: OrderByDirectionEnum
    }
    
    type PageControls {
        ticketFilterIsShow: Boolean!
    
    }
    
    type Query {
        ticketOrder: [TicketOrderBy]
        ticketFilter: TicketFilter
        filterShow:Boolean,
        pageControls:PageControls
   
    }
    type Mutation {
        setPageControls(payload:PageControls!):PageControls
        setTicketFilter(payload:TicketFilter!):TicketFilter
        setTicketOrder(payload:TicketOrder!):TicketOrder
    }
`

const cacheRedirects= {
    Query: {
        tickets: (_, { id }, { getCacheKey }) => {
           
            getCacheKey({ __typename: 'Ticket', id: id })
        },
    },
}

const resolvers = {
 /*   Query: {
       /!* draftTicketList: (parent, {id}, {cache}) => {
            //const key = context.getCacheKey({ __typename: 'DRAFT_TICKET_ITEM', id })
            const data = cache.readQuery({ query: DRAFT_TICKET_ITEM });
            console.log(data);
            //console.log(context.cache.extract()[key]);
            return {}
        },*!/
    /!*    ticketItem: (parent, {id}, {cache}) => {
            //const key = context.getCacheKey({ __typename: 'DRAFT_TICKET_ITEM', id })
            const data = cache.readQuery({ query: DRAFT_TICKET_ITEM });
            console.log(data);
            //console.log(context.cache.extract()[key]);
            return {}
        },
        book: (_, { id }, { getCacheKey }) => getCacheKey({ __typename: 'Book', id: id }),*!/

    },*/
    Mutation: {

        setTicketOrder: (root, { payload }, { cache }) => {
          
            const data = {
                ticketOrder:[{
                    ...payload,
                    __typename: 'TicketOrder'
                }]
            }


            cache.writeData({ data })
            return data
        },
        setTicketFilter: (root, { payload }, { cache }) => {
            
            const data = {
                ticketFilter:{
                    ...payload,
                    __typename: 'TicketFilter'
                },

            }
            cache.writeData({ data })
            return data
        },
        setPageControls: (root, { payload }, { cache }) => {
           
            const data = {
                pageControls:payload

            }
            cache.writeData({ data })
            return data
        },

    }
}

const clientState = {

    typeDefs,
    resolvers,

    onCacheInit: cache => {
        //console.log(cache);
        const data = {

            pageControls:{
                pageTitle:1234,
                ticketFilterIsShow:null,
                __typename:'PageControls'
            },
            ticketFilter:{
                //id: 0,

                is_request: false,
                is_free: false,
                is_auction: false,
                type: 1,
                price:{
                    min:null,
                    max:null,
                    __typename:'Price'
                },
                expiry_date:{
                    min:Date.now(),
                    max:null,
                    __typename:'ExpiryDate'
                },
                author_id: -1,
                categories: [],
                __typename:'TicketFilter'
            },
            ticketOrder:[
                {
                    field:'update_date',
                    direction:'desc',
                    __typename: 'TicketOrder'
                }
            ],

        }
        cache.writeData({ data })
    },
}


export default function(httpEndpoint){
    //console.log(context.store.$auth);
    return {
        // required
        $query: {
            loadingKey: 'loading',
            fetchPolicy: 'no-cache',
        },

        httpEndpoint: httpEndpoint,
        // optional
        // See https://www.apollographql.com/docs/link/links/http.html#options
        //getAuth : (token) =>  localStorage.getItem('auth._token.local'),
        httpLinkOptions: {
            //credentials: 'omit'
            credentials: 'same-origin',

            //credentials: 'include'
        },

   /*     ssrMode: true,
        cache: new InMemoryCache(),*/
        // You can use `wss` for secure connection (recommended in production)
        // Use `null` to disable subscriptions
        //wsEndpoint: httpEndpoint, // optional
        // LocalStorage token
        tokenName: 'apollo-token', // optional
        // Enable Automatic Query persisting with Apollo Engine
        persisting: false, // Optional
        // Use websockets for everything (no HTTP)
        // You need to pass a `wsEndpoint` for this to work
        websocketsOnly: false, // Optional
        ...clientState
    }
}
