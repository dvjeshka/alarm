



async function main(url,options={}){
    try {

        options = Object.assign(
            {
                //mode: 'same-origin',
                //credentials: "same-origin"
                //headers: {'Authorization': 'Bearer ' + localStorage.getItem('access_token')}
            }
            ,options
        )

      

        const response = await fetch(process.env.apiServer+url,options);
        let json = await response.json();
        if (response.ok) {
            return json
        }
        else {
          
            return Promise.reject(json)

        }

    } catch (error) {

        console.log('Возникла проблема с вашим fetch запросом: ',url, error.message);
    }
}

export default main




