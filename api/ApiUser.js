
import api from '@/api/ApiHelperParams.js'

import lodash_cloneDeep from "lodash/cloneDeep";
import _omit from "lodash/omit";
import mutationTicketEdit from "@/api/gql/mutationTicketEdit.gql";

export default {
    get(){
        return api('/api/user',{

        })
    },
    edit(payload) {
        //console.log(payload);
        payload = lodash_cloneDeep(payload)
        //payload.title='zzzzz'
        let {id} = payload
        let optimisticPayload = payload



        payload = _omit(payload,['__typename', 'author','id','update_date'])

        return this.$apollo.mutate({
            //client:'auth',
            mutation:mutationTicketEdit,
            variables: {
                id,
                payload
            },
            optimisticResponse:{
                tickets:{
                    edit:{
                        ...optimisticPayload,

                    },
                    __typename: "TicketMutation",
                },
            },
            /* update: (cache, { data:{tickets:{edit}} }) => {
                 console.log(cache);

                 let data = cache.readQuery({
                     query: QUERY_TICKET_ITEM,
                     variables:{filter:{id:id}}
                 })

                 console.log(data);

                 if(edit) {
                     try {


                         let t = cache.getCacheKey({ __typename: 'Ticket', id: id })

                         console.log(t);

                         return


                         if (categories.list) {
                             let findIndex = categories.list.findIndex(category => category.id === id)
                             if (findIndex!==-1) {
                                 let findCategory = categories.list[findIndex]
                                 findCategory.title = title
                                 findCategory.name = name
                                 findCategory.parent_id = parent_id
                                 /!*   this.$set('list',findIndex,findCategory)
                                    categories.list = list*!/

                                 categories.list.splice(findIndex, 1, findCategory)

                                 this.$store.commit('setCategoryList',lodash_cloneDeep(categories.list))

                             }

                         }

                         console.log(categories);
                         cache.writeQuery({
                             query: gqlCategoryList,
                             data: {
                                 categories
                             }
                         });
                     } catch (e) {
                         console.log(e);
                         // We should always catch here,
                         // as the cache may be empty or the query may fail
                     }
                 }
             }*/
        })
            .then(async ({errors,data:{tickets:{edit}}}) => {
              
                tost.call(this,errors,edit)
                //console.log(edit);
                //await sleep(3000)

                return edit


            }).catch((r)=>{
               

                let {networkError:{result:{errors}}} = r
                tost.call(this,errors)
            });
    },



    login(code){
        return api('/auth/login', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({ code: code })

        })
    },
    loginVK(){
        return process.env.apiServer + '/auth/vk/login';
    },
    logout(){
        return api('/api/logout')
    },


}
