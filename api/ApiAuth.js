import api from '@/modules/axios'
import apiHelperParams from '@/modules/apiHelperParams'

export default {
    selectBranch(params) {
        return api.post(process.env.VUE_APP_URL_MAIM, apiHelperParams('select-branch', params))
    },

}
