export default {
    data: () => ({
        windowW: 0,
        windowH: 0
    }),

    beforeDestroy() {
        if (typeof window !== 'undefined') {
            window.removeEventListener('resize', this.onResize, {
                passive: true
            })
        }
    },

    mounted() {
        this.onResize()
        window.addEventListener('resize', this.onResize, { passive: true })
    },

    methods: {
        onResize() {
            this.windowW = window.innerWidth
            this.windowH = window.innerHeight
        }
    },
    computed: {
        windowSize() {
            return [this.windowW, this.windowH]
        }
    }
}
