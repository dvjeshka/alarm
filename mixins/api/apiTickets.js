import gql from 'graphql-tag'
import lodash_isString from 'lodash/isString'
export default {
    watch:{
        '$route'(){
            this.$fetch()
        }
    },
    //fetchOnServer:false,
    async fetch(){
        const variables = ()=>{
            let category = this.$route.params.category
            if(category==='avtosighnalizatsii_pandora' || category==='avtosighnalizatsii_s_avtozapuskom') category='ustanovka_sighnalizatsii'
            return {
                filter: {
                    categories: [
                        this.$store.getters.categoriesNameToId('catalog_'+category)
                    ]
                }
            }
        }
        const {data} = await this.$apollo.query({
             query: gql`query tickets ($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                 tickets {
                     list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                         id
                         title
                         price
                         price_sale
                         images
                         specifications
                         named_id
                     }

                 }
             }`,
             skip () {
                 return this.$store.getters.categoriesNameToId('catalog_'+this.$route.params.category)==null && (this.componentPageCatalog==='PageCatalogDetail' || this.componentPageCatalog==='PageContacts')
             },
             variables:variables()
         })
        if(!data) return
        this.$store.commit('control/setPageData', ['tickets', []])
        let list = data?.tickets?.list
        if(list?.length) {
            list = list.map(item=>{
                if(lodash_isString(item.data)) {item.data = JSON.parse(item.data)}
                if(lodash_isString(item.specifications)) {item.specifications = JSON.parse(item.specifications)}
                return item
            })
            this.$store.commit('control/setPageData', ['tickets', list])
        }
      
    },
 /*   apollo: {

        apiTickets: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        id
                        title
                        price
                        price_sale
                        images
                        specifications
                        named_id
                    }
                    
                }
            }`,

            skip () {

                return this.$store.getters.categoriesNameToId('catalog_'+this.$route.params.category)==null && (this.componentPageCatalog==='PageCatalogDetail' || this.componentPageCatalog==='PageContacts')
            },
            variables() {
                console.log(this.$route.params.category); 
                let category = this.$route.params.category
                if(category==='avtosighnalizatsii_pandora' || category==='avtosighnalizatsii_s_avtozapuskom') category='ustanovka_sighnalizatsii'
                return {
                    filter: {
                        categories: [
                            
                            this.$store.getters.categoriesNameToId('catalog_'+category)
                        ]
                    }
                }


            },
            update: (data) => { //Missing apiIndexTop attribute on result
                return data
            },

            //fetchPolicy: 'cache-first',
            result({data, loading, networkStatus}) {
                if(!data) return
                this.$store.commit('control/setPageData', ['tickets', []])
                let list = data?.tickets?.list
                if(list?.length) {
                    list = list.map(item=>{
                        if(lodash_isString(item.data)) {item.data = JSON.parse(item.data)}
                        if(lodash_isString(item.specifications)) {item.specifications = JSON.parse(item.specifications)}
                        
                        return item
                    })
                   /!* item.data = JSON.parse(item.data)
                    console.log(item.data);*!/
                   
                    this.$store.commit('control/setPageData', ['tickets', list])


                    this.$nextTick(() => {
                        this.$nuxt?.$loading?.finish()
                    })
                }
              
            },
            // Error handling

        }
    }*/
}
