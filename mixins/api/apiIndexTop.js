import gql from 'graphql-tag'

export default {
    apollo: {

        apiIndexTop: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        id
                        title
                        description
                        images,
                    }
                    
                }
            }`,


            variables() {
                return {
                    filter: {
                        categories: [
                            this.$store.getters.categoriesNameToId('indexTop')
                        ]
                    }
                }


            },
            update: (data) => { //Missing apiIndexTop attribute on result
                return data
            },

            //fetchPolicy: 'cache-first',
            result({data, loading, networkStatus}) {
                if(!data) return
                this.$store.commit('control/setPageData', ['indexTop', data?.tickets?.list])
            },
            // Error handling

        }
    }
}
