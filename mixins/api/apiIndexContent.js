import gql from 'graphql-tag'

import lodash_isString from 'lodash/isString'

export default {
    
        apollo:{
            apiIndexContent: {

                query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                    tickets {
                        list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                            id
                            title
                        
                            data
                        }

                    }
                }`,


                variables() {
                    return {
                        filter: {
                            categories: [
                                this.$store.getters.categoriesNameToId('indexContent')
                            ]
                        },
                        order:[{
                            field:'update_date',
                            direction:'asc'
                        }]
                    }


                },
                update: (data) => { //Missing apiIndexTop attribute on result
                    return data
                },

                //fetchPolicy: 'cache-first',
                result({data, loading, networkStatus}) {
                    if(!data) return
                    let item = data?.tickets?.list[0]
                    if(item) {
                        if(lodash_isString(item.data)) {

                            item.data = JSON.parse(item.data)
                        }

                       
                        this.$store.commit('control/setPageMeta', item)
                    }
                },
                // Error handling

            }    
        }
        
    
}
