

import {ApiRequests} from '@/api/Api.js'

const stateModel = () => {
    return {
        name: undefined,
        phone: undefined,
        email: undefined,
        message:undefined
    }
}

export default {
    data() {
        return {
            isSend: false,
            model: stateModel()
        }
    },
    methods: {
        async submit() {
            const valid = await this.$validator.validateAll()


            if (!valid) return



            ApiRequests.add.call(this,this.model).then(()=>{
                this.isSend = true
                this.model= stateModel()
            })


            /*     axios(
                     {
                         headers: {
                             'content-type': 'application/json',
                            
                         },
                         method: 'get',
                         url: 'https://script.google.com/macros/s/AKfycbzgHstUsHoyAkZYsTM4rqYNQH6dKE46vuD2UE0ukRSvb7FDLak/exec',
                         params:{
                         'form_field_1':12,
                         'form_field_2':13,
                         'form_field_3':14,
                         'form_field_4':15,
                         }
                     },
                  
                     ).then((r)=>{
                     console.log(r,'finish');
                 })*/

            /*  let url ='https://script.google.com/macros/s/AKfycbzgHstUsHoyAkZYsTM4rqYNQH6dKE46vuD2UE0ukRSvb7FDLak/exec?form_field_1=1234'
              const response = await fetch(url,{
                  headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                  },
                  method: "GET",
                  
              });
              let json = await response.json();
              if (response.ok) {
                  console.log(json);
              }*/
        }
    }
}
